#!/usr/bin/python
"""
Photometry of K2 67P data
"""

import matplotlib
from astropy.io import fits
from os.path import join
import matplotlib.pyplot as plt
from astropy.wcs import WCS
from astropy.time import Time
import numpy as np
from astropy import visualization
import astropy.units as u
from photutils import EllipticalAperture, RectangularAperture, aperture_photometry, centroid_com
import argparse
from orm import db, Cadence, Aperture, Photometry, Ephemeris
import callhorizons
from reproject import reproject_interp
from matplotlib import rc

rc('text', usetex=True)

def master_astrom(files, wcs):
    """Register frames to common WCS

    Parameters
    ----------
    files : list
        list of FITS files

    wcs : astropy.wcs.WCS
        WCS object of the master frame
    """
    arr_list = []
    for f,w in files:
        print(f)
        hdu = fits.open(f)[1]
        new_image, footprint = reproject_interp((hdu.data, w), wcs,
                                                shape_out=hdu.data.shape)
        arr_list.append(new_image)
    master = np.median(arr_list, axis=0)
    np.save('master_{0}'.format(args.channel), master)
    return master

# Parsing command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-c', '--channel', default=69, type=int, choices=(69, 70),
                    help='channel number')
args = parser.parse_args()

db.connect()

datadir = '/home/miguel/project/67P/K2/data'

backgr = fits.open('average_ch{0}.fits'.format(args.channel))[0].data
# backgr = fits.open('bk_ch{0}.fits'.format(args.channel))[0].data

theta = {69:115, 70:70}
theta_px = {69:19, 70:-19}
frames = {69: range(132310, 132599), 70: range(132010, 132309)}

master_frames = np.load('frames_{0}.npy'.format(args.channel))

# random images for master frame
files = [(join(datadir, 'k2mosaic-c102-ch{0}-cad{1}.fits'.format(args.channel, i)),
          WCS(fits.open(join(datadir, 'output',
            'k2mosaic-all-c102-ch{0}-cad{1}.new'.format(args.channel, i)))[0].header))
         for i in master_frames[::-1][2:20]]

wcs_master = files[0][1]

# chose one of the lines below
# master = master_astrom(files, wcs_master)
# master = np.load('master_{0}.npy'.format(args.channel))

# ap = Aperture.create(type='elliptical',pixel=False,a=13,b=8)
# ap = Aperture.create(type='elliptical',pixel=True,a=2.8,b=1)
# ap = Aperture.create(type='rectangular',pixel=True,a=5.68,b=.84*2)
ap = Aperture.get(type='rectangular',pixel=True)

fig = plt.figure()
ax = fig.gca()

horizons = callhorizons.query('900670')
long_cad = 29.424 # min
# half long-cadence in days
diff = long_cad/2./60./24

for cadence in frames[args.channel]:
    i = join(datadir, 'k2mosaic-c102-ch{0}-cad{1}.fits'.format(args.channel, cadence))
    j = join(datadir, 'output', 'k2mosaic-all-c102-ch{0}-cad{1}.new'.format(args.channel, cadence))
    cad = Cadence.get(Cadence.cadence == cadence)

    timeobj = Time(cad.mjd, format='mjd')
    horizons.set_discreteepochs([timeobj.jd-diff, timeobj.jd, timeobj.jd+diff])
    horizons.get_ephemerides('500@-227')

    hdus = fits.open(i)
    # wcs_an = WCS(fits.open(j)[0].header)
    wcs_master = WCS(fits.open(j)[0].header)

    transform = (visualization.LogStretch() +
                     visualization.ManualInterval(vmin=1, vmax=45))

    flux = hdus[1].data
    flux -= backgr
    # flux, footprint = reproject_interp((hdus[1].data, wcs_an), wcs_master,
    #                                         shape_out=hdus[1].data.shape)
    # flux -= master

    ax = plt.subplot(projection=wcs_master)

    plt.imshow(transform(flux), aspect='equal', origin='lower')
    # plt.imshow(transform(flux), aspect='equal', origin='lower')
              # interpolation='nearest', cmap="gray")#, norm=NoNorm())
    plt.grid(color='gray', alpha=0.5, ls='dotted')
    radius = 18 # pix
    # pixcrd = wcs.wcs_world2pix(*geom.radec, 0)
    pixcrd = wcs_master.all_world2pix(horizons['RA']*u.degree,
                                      horizons['DEC']*u.degree, 0)
    # print(pixcrd)
    # i = int(pixcrd[0])
    # x1, y1 = centroid_com(flux[:,i-8:i+10])
    # x1 += i-8
    ax.set_xlim(pixcrd[0][1] - radius, pixcrd[0][1] + radius)
    ax.set_ylim(pixcrd[1][1] - radius*0.7, pixcrd[1][1] + radius*0.7)
    ax.set_xlabel('RA')
    # ax.set_xticklabels([])
    # ax.yaxis.labelpad = -200
    ax.set_ylabel('DEC')
    mid = horizons[1]
    Ephemeris.create(cadence=cadence, channel=args.channel, ra=mid['RA'],
                     dec=mid['DEC'], delta=mid['delta'],
                     rh=mid['r'], alpha=mid['alpha'],
                     pix_x=pixcrd[0][1], pix_y=pixcrd[1][1],
                     pix_x0=pixcrd[0][0], pix_y0=pixcrd[1][0],
                     pix_x1=pixcrd[0][2], pix_y1=pixcrd[1][2],
                     method='all')

    ax.plot(*pixcrd, 'r', alpha=0.8)
    # ax.plot([pixcrd[0]], [pixcrd[1]], '.b', markersize=4, alpha=0.8)
    # plot centroid
    # ax.plot([x1], [y1], '.g', markersize=4)
    if args.channel == 70:
        ax.invert_xaxis()
    # plt.colorbar()

    for aper in Aperture.select():
    #     # apertures = EllipticalAperture([pixcrd, (x1, y1)],
    #     #                                a=aper.a, b=aper.b,
    #     #                                theta=theta_px[args.channel]*np.pi/180)
        apertures = RectangularAperture((pixcrd[0][1], pixcrd[1][1]),
                                       w=aper.a, h=aper.b,
                                       theta=theta_px[args.channel]*np.pi/180)
    apertures.plot(color='k', lw=1.5, alpha=0.2)

    # plt.title('channel {0} {1}'.format(args.channel, timeobj.iso[0:19]))
    plt.savefig(join('figures', 'mosaic_ch{0}_{1}_diff.png'.format(args.channel,
                                                                  cadence)),
                bbox_inches='tight')
    plt.close()

    mask = np.isnan(flux)
    phot_table = aperture_photometry(flux, apertures, mask=mask)
    print(phot_table)
    # data_source = [{'cadence': cadence, 'aperture': 3, 'sum':i, 'type':'an'}
    #                 for i in phot_table['aperture_sum'].data]
    # with db.atomic():
    #     Photometry.insert_many(data_source).execute()
    # Photometry.create(cadence=cadence, aperture=3,
    #                   sum=phot_table['aperture_sum'].data,
    #                   type='an_diff')

    # plt.colorbar()


db.close()
