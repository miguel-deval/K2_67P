#!/usr/bin/python

from peewee import (SqliteDatabase, Model, FloatField, TextField, IntegerField,
ForeignKeyField, BooleanField)

db = SqliteDatabase('67P.db')

class BaseModel(Model):
    class Meta:
        database = db

class Aperture(BaseModel):
    a = FloatField()
    b = FloatField(null=True)
    phase = FloatField(null=True)
    type = TextField(null=True, choices=('circular', 'elliptical', 'rectangular'))
    pixel = BooleanField()

    class Meta:
        db_table = 'aperture'

class Cadence(BaseModel):
    cadence = IntegerField(unique=True)
    time = FloatField()
    timecorr = FloatField(null=True)
    mjd = FloatField(null=True)

    class Meta:
        db_table = 'cadence'

class Ephemeris(BaseModel):
    cadence = ForeignKeyField(rel_model=Cadence, to_field='cadence')
    ra = FloatField(null=True)
    dec = FloatField(null=True)
    delta = FloatField(null=True)
    rh = FloatField(null=True)
    alpha = FloatField(null=True)
    channel = IntegerField(null=True)
    method = TextField(null=True)
    pix_x = FloatField(null=True)
    pix_y = FloatField(null=True)
    pix_x0 = FloatField(null=True)
    pix_y0 = FloatField(null=True)
    pix_x1 = FloatField(null=True)
    pix_y1 = FloatField(null=True)

    class Meta:
        db_table = 'ephemeris'

class Photometry(BaseModel):
    aperture = ForeignKeyField(db_column='aperture_id', null=True, rel_model=Aperture, to_field='id')
    cadence = ForeignKeyField(db_column='cadence_id', null=True,
                              rel_model=Cadence, to_field='cadence')
    type = TextField(null=True, choices=('an', 'an_hor', 'ffi', 'centroid'))
    sum = FloatField(null=True)
    error = FloatField(null=True)

    class Meta:
        db_table = 'photometry'

class Residual(BaseModel):
    cadence = ForeignKeyField(rel_model=Cadence, to_field='cadence')
    reference = IntegerField(null=True)
    channel = IntegerField(null=True)
    median = FloatField(null=True)

    class Meta:
        db_table = 'residual'

# db.connect()
# db.create_tables([Aperture, Cadence, Ephemeris, Photometry, Residual])
# db.close()
