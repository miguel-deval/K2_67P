#!/usr/bin/python
"""
Data for photometry comparison figure
"""

import pandas as pd
import sqlite3
import matplotlib.pyplot as plt
from os.path import join
from datetime import datetime, timedelta
from astropy.time import Time
from astropy.stats import LombScargle, sigma_clip
import numpy as np
import callhorizons
# from mskpy import getspiceobj, Earth

def date_mjd(x):
    """
    Convert time in MMDD.dd format to MJD - 5e4
    """
    month = int(x//100)
    day = x%100
    date = datetime(2015, month, 1) + (day-1)*timedelta(1)
    return Time(date).mjd - 5e4


def fors_mjd(x):
    """
    Convert time in YYYY-MM-DD format to MJD - 5e4
    """
    date = datetime.strptime(x, '%Y-%b-%d')
    return Time(date).mjd - 5e4


def mag(x):
    """
    R magnitude
    """
    t = 29.424 # min
    return -2.5*np.log10(x/t/60) + 18.25


def mR(row):
    """
    magnitude at unit delta and zero phase angle
    """
    beta = 0.02
    return row['R'] - 5*np.log10(row['delta']) - beta*row['alpha']



datadir = '/home/miguel/project/67P/K2/'

con = sqlite3.connect('67P.db', detect_types=sqlite3.PARSE_DECLTYPES)

df = pd.read_sql('''SELECT photometry.sum, photometry.error, photometry.type,
                 cadence.cadence, cadence.time, cadence.timecorr,
                 photometry.aperture_id, ephemeris.delta, ephemeris.alpha,
                 ephemeris.rh
                 FROM photometry
                 INNER JOIN cadence
                    ON photometry.cadence_id=cadence.cadence
                 INNER JOIN ephemeris
                    ON ephemeris.cadence_id=photometry.cadence_id''', con)

con.close()

# plt.scatter(df[df['type'] == 'ffi']['time'], df[df['type'] == 'ffi']['sum'],
#             marker='.', color='r', label="FFI WCS")
# plt.scatter(df[df['type'] == 'an']['time'], df[df['type'] == 'an']['sum'],
#             marker='.', color='b', label="astrometry.net")
# plt.scatter(df[df['type'] == 'centroid']['time'], df[df['type'] == 'centroid']['sum'],
#             marker='.', color='g', label="centroid")
lc = df[df['type'] == 'an_med'].copy()

t = lc['time']
y = lc['sum']

filtered_y = sigma_clip(y, sigma=5)
mask = filtered_y.mask
print(len(y), len(y[mask]))

plt.scatter(t, filtered_y, marker='.', color='g', label="rectangular")
# plt.legend()
plt.axvline(x=2812.478789954, ls='--')
# plt.ylim(50, 100)
plt.xlabel('BKJD')
plt.ylabel('Kepler counts')
plt.savefig(join(datadir, 'paper', 'figures', 'lightcurve.png'))
plt.close()

# Lomb-Scargle periodogram
frequency = np.linspace(0.1, 10, 2000)
power = LombScargle(t, y).power(frequency)
# frequency, power = LombScargle(t, y).autopower()
power = LombScargle(t[~mask], filtered_y[~mask]).power(frequency)
plt.plot(frequency, power)
plt.xlim(0.2,10)
# plt.ylim(0,0.1)
plt.axvline(x=24/12.4, ls='--')
plt.xlabel('Frequency [cycle/days]')
plt.ylabel('Lomb-Scargle power')
plt.savefig(join(datadir, 'paper', 'figures', 'lombscargle.png'))

import sys
sys.exit()

TIMSLICE = 3
BJDREFI = 2454833
BJDREFF = 0
time_slice_correction = (0.25 + 0.62*(5- TIMSLICE))/86400
bjd = lc['time'].values + BJDREFI
jd = bjd - lc['timecorr'] + time_slice_correction

lc['mjd'] = jd[~mask] - 2450000.5

lc['tel'] = 'K2'

lc['R'] = mag(lc['sum'])

# http://stackoverflow.com/a/16354730/605436
lc['mR'] = lc.apply(mR, axis=1)

lc[['mjd', 'mR', 'tel']][~mask].to_csv(join(datadir, 'paper', 'k2.dat'), sep=' ',
                                              index=False, float_format='%.3f')

print(np.min(lc['rh']))
print(np.max(lc['rh']))

lc[~mask][['cadence', 'time', 'R', 'mR', 'rh', 'delta', 'alpha']][:20].to_csv(join(datadir, 'paper',
                                                   'table.txt'), sep='&',
                                              index=False, header=False,
                                              line_terminator='\\\\\n',
                                              float_format='%.3f')

# process ground-based robotic data
df = pd.read_csv(join(datadir, 'paper', 'photometry.dat'), delimiter="\s+")

df['mjd'] = df['date'].apply(date_mjd)

df[['mjd', 'r', 'Delta', 'mag', 'R', 'Afrho', 'tel']].to_csv(join(datadir,
                                                                  'paper',
                                                                  'photometry_2.dat'),
                                                             index=False, sep=' ')

# sort prediction data
df = pd.read_csv(join(datadir, 'paper', 'dataset.csv'), delimiter="\s+",
                 names=['MJD','mag'])

df.sort_values(by='MJD', inplace=True)

horizons = callhorizons.query('900671')
horizons.set_discreteepochs(df['MJD'].values + 2450000.5)
horizons.get_ephemerides(568)
delta = horizons['delta']
alpha = horizons['alpha']

# cg = getspiceobj('1000012')
# geom = Earth.observe(cg, df['MJD'].values[:10] + 2400000.5)

print(delta)
print(alpha)

# convert to magnitude at unit delta and zero phase angle
beta = 0.02
df['H'] = df['mag'] - 5*np.log10(delta) - beta*alpha

print(df.columns)
df.to_csv(join(datadir, 'paper', 'prediction.dat'), index=False, sep=' ')

# process FORS data

df = pd.read_csv(join(datadir, 'paper', 'fors.txt'), delimiter="\s+")

df['mjd'] = df['date'].apply(fors_mjd)
df['tel'] = 'FORS'

df.to_csv(join(datadir, 'paper', 'fors.dat'), index=False, sep=' ')
