#!/usr/bin/python
"""
Median of subtracted images to find randomly matching images
"""

import astropy.units as u
from astropy.io import fits
from os.path import join
from orm import db, Ephemeris, Residual, Cadence, Aperture, Photometry
import argparse
import numpy as np
import matplotlib.pyplot as plt
from astropy.wcs import WCS
from astropy import visualization
from photutils import RectangularAperture, aperture_photometry
# from astropy.stats import mad_std

datadir = '/home/miguel/project/67P/K2/data'
frames = {69: range(132310, 132599), 70: range(132010, 132309)}
theta_px = {69:19, 70:-19}

# Parsing command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-c', '--channel', default=69, type=int, choices=(69, 70),
                    help='channel number')
args = parser.parse_args()

for cadence in frames[args.channel]:
    filename = join(datadir, 'k2mosaic-c102-ch{0}-cad{1}.fits'.format(args.channel, cadence))
    hdus = fits.open(filename)
    flux = hdus[1].data
    # astronomy.net WCS
    j = join(datadir, 'output', 'k2mosaic-all-c102-ch{0}-cad{1}.new'.format(args.channel, cadence))
    wcs = WCS(fits.open(j)[0].header)
    median = []
    mad = []
    ref_list = [e for e in frames[args.channel] if e not in range(cadence-20, cadence+20)]
    for ref in ref_list:
        filename = join(datadir, 'k2mosaic-c102-ch{0}-cad{1}.fits'.format(args.channel, ref))
        hdus = fits.open(filename)
        # median.append(np.abs(np.nanmedian(flux - hdus[1].data)))
        mad.append(np.nanstd(flux - hdus[1].data))
    # find cadence that gives lowest median residual
    # refcad = ref_list[np.argmin(median)]
    refcad = ref_list[np.argmin(mad)]
    filename = join(datadir, 'k2mosaic-c102-ch{0}-cad{1}.fits'.format(args.channel, refcad))
    hdus = fits.open(filename)

    ep = Ephemeris.get(Ephemeris.cadence_id == cadence)
    cad = Cadence.get(Cadence.cadence == cadence)

    transform = (visualization.LogStretch() +
                     visualization.ManualInterval(vmin=1, vmax=45))

    flux -= hdus[1].data

    ax = plt.subplot(projection=wcs)

    plt.imshow(transform(flux), aspect='equal', origin='lower')
    plt.grid(color='gray', alpha=0.5, ls='dotted')
    radius = 18 # pix
    # pixcrd = wcs.all_world2pix(ep.ra*u.degree, ep.dec*u.degree, 0)
    ax.set_xlim(ep.pix_x - radius, ep.pix_x + radius)
    ax.set_ylim(ep.pix_y - radius*0.7, ep.pix_y + radius*0.7)
    ax.set_xlabel('RA')
    ax.set_ylabel('DEC')
    # ax.plot([ep.pix_x], [ep.pix_y], '.r')
    ax.plot([ep.pix_x0, ep.pix_x, ep.pix_x1],
            [ep.pix_y0, ep.pix_y, ep.pix_y1],
            'r', markersize=4, alpha=0.8)
    if args.channel == 70:
        ax.invert_xaxis()

    for aper in Aperture.select():
        apertures = RectangularAperture((ep.pix_x, ep.pix_y),
                                       w=aper.a, h=aper.b,
                                       theta=theta_px[args.channel]*np.pi/180)
    apertures.plot(color='k', lw=1.5, alpha=0.2)

    # plt.title('channel {0} {1}'.format(args.channel, timeobj.iso[0:19]))
    plt.savefig(join('figures', 'mosaic_ch{0}_{1}_med.png'.format(args.channel,
                                                                  cadence)),
                bbox_inches='tight')
    plt.close()

    mask = np.isnan(flux)
    phot_table = aperture_photometry(flux, apertures, mask=mask)

    print(phot_table)

    Photometry.create(cadence=cadence, aperture=3,
                      sum=phot_table['aperture_sum'].data,
                      type='an_med')
