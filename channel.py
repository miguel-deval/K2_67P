#!/usr/bin/python
"""
Print information about TPFs and create cadence table
"""

from astropy.io import fits
from os.path import join
import numpy as np
import orm

def mjd(hdu):
    """
    Calculate MJD
    """
    TIMSLICE = hdu.header['TIMSLICE']
    BJDREFI = hdu.header['BJDREFI']
    BJDREFF = hdu.header['BJDREFF']
    time_slice_correction = (0.25 + 0.62*(5- TIMSLICE))/86400
    bjd = hdu.data['TIME'] + BJDREFI + BJDREFF
    jd = bjd - hdu.data['TIMECORR'] + time_slice_correction
    return jd-2400000.5

datadir = '/home/miguel/project/67P/K2/data'
with open(join(datadir, 'list-tpf-69.txt')) as f:
    files = f.read().splitlines()

# for i in files:
#     hdus = fits.open(i)
#     # print(hdus[0].header['MODULE'], hdus[0].header['CHANNEL'])
#     if hdus[1].header['TIMSLICE'] != 3:
#         print(i)

orm.db.connect()

orm.db.create_table(orm.Cadence)

for i in files[:1]:
    hdus = fits.open(i)
    cadence = hdus[1].data['CADENCENO']
    time = hdus[1].data['TIME']
    timecorr = hdus[1].data['TIMECORR']
    quality = hdus[1].data['QUALITY']
    # mask = (cadence > 132009) & (cadence < 132310)
    mask = (cadence > 132009) & (cadence < 132600)
    print(np.where((quality[mask] > 0) & (quality[mask] < 66000)))

    data_source = [{'cadence': i[0], 'time': i[1], 'timecorr':i[2], 'mjd':i[3]}
                    for i in zip(cadence[mask], time[mask], timecorr[mask],
                                 mjd(hdus[1])[mask])]
    with orm.db.atomic():
        orm.Cadence.insert_many(data_source).execute()

orm.db.close()
