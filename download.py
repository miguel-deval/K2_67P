#!/usr/bin/python

"""
Download TPFs from GO10904 proposal using requests
"""

import pandas as pd
from os.path import join
import requests

k2dir = '/home/miguel/project/67P/K2/data'
base_url = "http://archive.stsci.edu/pub/k2/target_pixel_files/c102/200000000/"
tpf = "ktwo{0}-c102_lpd-targ.fits.gz"


df = pd.read_csv(join(k2dir, 'GO10904-targets.csv'), usecols=[0])

def download_file(i):
    """
    Download TPF from MAST

    Parameters
    ----------
    i : int
        EPIC ID

    Returns
    -------
    local_filename : str
        FITS file
    """
    filename = tpf.format(i)
    url = join(base_url, '{0}000'.format(str(i)[4:6]), filename)
    local_filename = join(k2dir, filename)
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            # filter out keep-alive new chunks
            if chunk:
                f.write(chunk)
    return local_filename


for i in df['EPIC ID']:
    print(tpf.format(i))
    download_file(i)
