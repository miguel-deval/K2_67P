#!/usr/bin/python

import numpy as np
from astropy.io import fits
from os.path import join
import matplotlib.pyplot as plt
from astropy.wcs import WCS
from astropy import visualization
from matplotlib import rc

# rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

k2dir = '/home/miguel/project/67P/K2'
datadir = join(k2dir, 'data')


hdus = fits.open(join(datadir, 'k2mosaic-all-c102-ch69-cad132309.fits'))
hdus2 = fits.open(join(datadir, 'k2mosaic-all-c102-ch70-cad132309.fits'))

# from horizons
psang, psamv = 111.408, 297.452

wcs = WCS(hdus[1].header)

transform = (visualization.LogStretch() +
                     visualization.ManualInterval(vmin=1, vmax=800))

plt.figure(figsize=(10,6), dpi=200)

ax = plt.subplot(projection=wcs)

shape = hdus[1].data.shape

flux = np.concatenate((hdus[1].data[:,:-20], hdus2[1].data[:,::-1][:,20:]), axis=1)

plt.axvline(x=shape[1]-20, color="gray", alpha=.5, linewidth=1)

# NE axes
ax.arrow(shape[1]-20, shape[0]//2, 28.12, 200, head_width=5, head_length=20,
         fc='c', ec='c', alpha=0.5)
ax.arrow(shape[1]-20, shape[0]//2, -200, 28.12, head_width=5, head_length=20,
         fc='c', ec='c', alpha=0.5)
plt.text(shape[1],shape[0]//2+225, 'N', color='c', alpha=0.5)
plt.text(shape[1]-265,shape[0]//2+20, 'E', color='c', alpha=0.5)


# projected vectors
psang -= 180 + 8
x0 = -np.sin(psang*np.pi/180)
y0 = np.cos(psang*np.pi/180)
ax.arrow(shape[1]-20, shape[0]//2, 100*x0, 100*y0,
         # linestyle='dotted',
         head_width=5, head_length=20,
         fc='g', ec='g', alpha=0.5,
         length_includes_head=True)
plt.text(shape[1]-20+100*x0+6,shape[0]//2+100*y0-7, r'$\odot$', color='g', alpha=0.5)

psamv -= 180 + 8
x0 = -np.sin(psamv*np.pi/180)
y0 = np.cos(psamv*np.pi/180)
ax.arrow(shape[1]-20, shape[0]//2, 100*x0, 100*y0, head_width=5, head_length=20,
         fc='b', ec='b', alpha=0.5,
         length_includes_head=True)
plt.text(shape[1]-20+100*x0-25, shape[0]//2+100*y0-10, r'$v$', color='b', alpha=0.5)

plt.plot([0, 1000, 1500, 2220], [239, 575, 746, 998], 'r', linewidth=0.8, alpha=.6)

plt.imshow(transform(flux), origin="lower", cmap='gray',
           interpolation="nearest")

ax.set_xlim(-0.5, flux.shape[1] - 0.5)

plt.grid(color='gray', alpha=0.5, ls='dotted')

plt.xlabel('RA')
plt.ylabel('Dec')

plt.savefig(join(k2dir, 'paper', 'figures', 'mosaic.png'), bbox_inches='tight')

# scipy.misc.imsave('outfile.png', hdus[1].data)
