Download the data for this program from MAST using the script `download.py`.

List of all target pixel files for campaign 102 channel 69

```
k2mosaic tpflist 102 69 > list-tpf-69-all.txt
k2mosaic mosaic list-tpf-69-all.txt --cadence 132309..132599 -o k2mosaic-all-c
k2mosaic movie moviel.txt --cadence 132309..132599 -o k2mosaic-all-c
```

campaign 102 cadences
array([129231, 129232, 129233, ..., 132612, 132613, 132614], dtype=int32)

frame range
frames = {69: range(132310, 132599), 70: range(132010, 132309)}

astrometry.net only works with sextractor

